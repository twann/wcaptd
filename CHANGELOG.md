# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.5] - 2023-11-18

### Added
- Log each time a known domain is requested and calculate a presence score for every known domain
- Add a column in the database to see how the domain was discovered
### Changed
- Change project name from `wcapt` to `wcaptd`
- Improve help page

## [0.0.4] - 2023-11-13

### Added
- Allow to export database as CSV
### Changed
- Improve verbosity in comments when CNAME alias is found

## [0.0.3] - 2023-11-12

### Added
- Allow to discover domains based on regex patterns

## [0.0.2] - 2023-11-12

### Fixed
- Do not treat the same packets twice

## [0.0.1] - 2023-11-12

### Added
- Initial release

[unreleased]: https://codeberg.org/twann/wcaptd/src/branch/main
[0.0.5]: https://codeberg.org/twann/wcaptd/releases/tag/0.0.5
[0.0.4]: https://codeberg.org/twann/wcaptd/releases/tag/0.0.4
[0.0.3]: https://codeberg.org/twann/wcaptd/releases/tag/0.0.3
[0.0.2]: https://codeberg.org/twann/wcaptd/releases/tag/0.0.2
[0.0.1]: https://codeberg.org/twann/wcaptd/releases/tag/0.0.1
