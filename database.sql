CREATE TABLE IF NOT EXISTS "domains" (
    "host" TEXT NOT NULL UNIQUE,
    "brand"  TEXT,
    "product"   TEXT,
    "type"  TEXT NOT NULL,
    "comment"   TEXT,
    "date"  TEXT NOT NULL,
    "false_pos" INT NOT NULL DEFAULT 0,
    "presence"  INT NOT NULL DEFAULT 1,
    "creator"   TEXT,
    PRIMARY KEY("host")
);
CREATE TABLE IF NOT EXISTS "regex" (
    "pattern"   TEXT NOT NULL UNIQUE,
    "brand"  TEXT,
    "product"   TEXT,
    "type"  TEXT NOT NULL,
    "comment"   TEXT,
    "date"  TEXT NOT NULL,
    PRIMARY KEY("pattern")
);
